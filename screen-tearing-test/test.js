let line_distance = 10
let line_speed = 0.5

let canvas = document.getElementById("canvas")
let ctx = canvas.getContext('2d')

let isPaused = true

function clear(){
  ctx.clearRect(0, 0, canvas.width, canvas.height)
}

function draw(offset = 0){
  offset = offset % line_distance

  clear()
  for(let x = offset; x < canvas.width; x += line_distance){
    ctx.beginPath()
    ctx.moveTo(x, 0)
    ctx.lineTo(x, canvas.height)
    ctx.stroke()
  }

  if(!isPaused){
    requestAnimationFrame(() => draw(offset + line_speed))
  } else {
    requestAnimationFrame(() => clear())
  }
}

function pause(){
  isPaused = true
}

document.getElementById("start").addEventListener("click", () => {
  isPaused = false
  draw()
})

canvas.addEventListener("click", () => {
  isPaused = true
})

draw()