export default {
  sqrt(value){
    return Math.sqrt(value)
  },

  square(value){
    return Math.pow(value, 2)
  },

  pi(value){
    return Math.PI
  },

  sin(value){
    return Math.sin(value)
  },

  cos(value){
    return Math.cos(value)
  },

  tan(value){
    return Math.tan(value)
  }
}