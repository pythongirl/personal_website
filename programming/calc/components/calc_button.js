export default {
  name: "calc-button",
  props: ["label", "func", "pos"],
  computed: {
    onClick(){
      return () => {
        this.$root.functions[this.func](this.$root)()
        Haptics.vibrate(75)
      } 
    }
  },
  template: `
    <div class="button-container" @click="onClick" :style="{gridArea: pos}">
      {{ label }}
    </div>
  `
}