import CalcButton from "./components/calc_button.js"
import CalcFunctions from "./calc_functions.js"
import CalcOperations from "./calc_bin_operations.js"

var app = new Vue({
  el: "#app",
  data: () => ({
    functions: CalcFunctions,
    input: null,
    stack: [0],
    overwrite: true,
  }),
  computed: {
    input_value: {
      get(){ return parseFloat(this.input) },
      set(value){
        this.input = value.toString()
      }
    },
    display_value(){
      if(this.input == null){
        return this.stack[this.stack.length - 1]
      } else {
        return this.input
      }
    }
  },
  methods: {
    applyOperation(){
      if(this.operation != "" && this.input != null){
        this.last_value = CalcOperations[this.operation](this.last_value, this.input_value)
      }
    }
  },
  components: {
    CalcButton
  }
})

window.app = app