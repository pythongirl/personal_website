import document

document.body = document.getElementsByTagName("body")[0]

def newElement(tag_name):
  return document.createElement(tag_name)

p = newElement("ul")
for x in range(10):
  a = newElement("li")
  a.innerText = str(x)
  p.appendChild(a)

document.body.appendChild(p)


