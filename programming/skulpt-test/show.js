function remapFuncToJs(f){
  return () => (Sk.misceval.callsim(f));
}

var $builtinmodule = function(name){
  return {
    show(x){
      console.log(Sk.ffi.remapToJs(x))
    },
    timeout(f, t){
      setTimeout(remapFuncToJs(f), Sk.ffi.remapToJs(t))
    }
  }
}