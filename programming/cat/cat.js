'use strict';

(function(){

	function run(code){
		console.log(code)
	}

	function init(){
		var tags = Array.from(document.getElementsByTagName('script')).filter(e => e.type == "text/cat")

		for(var t = 0; t < tags.length; t++){
			if(tags[t].src){
				var request = new XMLHttpRequest()
				request.addEventListener("load", (e) => {
					run(request.responseText)
				}, false)
				request.open("GET", tags[t].src)
				request.send()
			} else {
				run(tags[t].text)
			}
		}

	}

	function main(){
		window.addEventListener('load', init);
	}

	main();

})()
