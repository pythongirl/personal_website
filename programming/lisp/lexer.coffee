export default (input) ->
  output = []

  i = 0
  while i < input.length
    slice = input[i..]

    # Whitespace
    if slice.match /^\s/
      i++
      continue

    # Punctuation
    match_punctuation = slice.match /^(\(|\)|\[|\]|{|}|#{)/
    if match_punctuation
      i += match_punctuation[0].length
      yield {type: "punc", value: match_punctuation[0]}
      continue
  
    # Numbers
    float_match = slice.match /^[-+]?\d*(\.\d+)?([eE][+-]?\d+)?/
    if float_match[0]
      float_literal = float_match[0]
      float_value = parseFloat float_literal

      i += float_match[0].length
      yield {type: "number", value: float_value}
      continue
    
    # Strings
    if slice[0] == '"'
      value = ""
      escaped = no


      for p in [1..slice.length]
        c = slice[p]

        if !escaped
          if c == '\\'
            escaped = yes
          else if c == '"'
            break
          else if c == '\n'
            throw new Error "newline in string"
          else
            value += c
          
        else
          escape_chars = 
            'n': '\n'
            '\n': '\n'
            't': '\t'
            'r': '\r'
            '"': '"'
            '\\': '\\' 
          value += escape_chars[c]
          escaped = no

        if p == slice.length-1
          throw new Error "Unfinished string"

      i += p + 1
      yield {type: "string", value: value}
      continue

    # Symbols
    symbol_match = slice.match /^[^\s\(\)#\[\]{}]+/
    if symbol_match
      i += symbol_match[0].length
      yield {type: "symbol", value: symbol_match[0]}
      continue