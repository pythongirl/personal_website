(() => {
  var icon_link = document.createElement("link")
  icon_link.rel = "icon"
  icon_link.type = "image/png"
  icon_link.href = "/images/icons/small/folder.png"
  document.head.appendChild(icon_link)
})()