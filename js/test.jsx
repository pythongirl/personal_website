/* exported template */
let template = (title, body) => <>
  <h3>{title}</h3>
  <p>{body}</p>
</>