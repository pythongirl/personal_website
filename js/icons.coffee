# From https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
copyToClipboard = (str) ->
  el = document.createElement "textarea"
  el.value = str
  el.setAttribute "readonly", ""
  el.style.position = "absolute"
  el.style.left = "-9999px"
  document.body.appendChild el
  el.select
  document.execCommand "copy"
  document.body.removeChild el


Vue.component "icon-display",
  props: ["iconName", "iconSize"]
  computed:
    iconImage: -> "/images/icons/#{ if this.iconSize is "small" then "small/" else ""}#{this.iconName}.png"
    classes: -> this.iconSize + " icon-preview"
  mounted: ->
    this.$el.addEventListener "click", ->
      copyToClipboard this.iconImage
      snackbar = document.querySelector "#snackbar" 
      snackbar.classList.add "visible"
      setTimeout -> snackbar.classList.remove "visible", 2000

  template: "<div class='icon-display'>\
      <img :class='classes' :src='iconImage' :title='iconName'/>\
    </div>"


large_icons = ["a", "alert.black", "alert.red", "back", "ball.gray",
  "ball.red", "binary", "binhex", "blank", "bomb", "box1", "box2", "broken",
  "burst", "c", "comp.blue", "comp.gray", "compressed", "continued", "dir",
  "diskimg", "down", "dvi", "folder", "folder.sec", "forward", "generic",
  "generic.red", "generic.sec", "hand.right", "hand.up", "image1", "image2",
  "image3", "index", "layout", "left", "link", "movie", "p", "patch", "pdf", 
  "pie0", "pie1", "pie2", "pie3", "pie4", "pie5", "pie6", "pie7", "pie8",
  "portal", "ps", "quill", "right", "screw1", "screw2", "script", "sound1",
  "sound2", "sphere1", "sphere2", "svg", "tar", "tex", "text", "transfer",
  "unknown", "uu", "uuencoded", "world1", "world2", "xml"]

small_icons = ["back", "binary", "binhex", "blank", "broken", "burst",
  "comp1", "comp2", "compressed", "continued", "doc", "folder", "folder2",
  "forward", "generic", "generic2", "generic3", "image", "image2", "index",
  "key", "movie", "patch", "ps", "sound", "sound2", "tar", "text", "transfer",
  "unknown", "uu"]


app = new Vue
  name: "Apache Icons"
  el: "#app"
  computed:
    icons: -> (if this.small_icons then small_icons else large_icons).filter((name) -> name.match this.filter)
  data: -> {filter: "", small_icons: no}


