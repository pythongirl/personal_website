(() => {
  var viewport_tag = document.createElement("meta")
  viewport_tag.name = "viewport"
  viewport_tag.content = "width=device-width, initial-scale=1.0"
  document.head.appendChild(viewport_tag)
})()
