let prime_generator = (function* primes(){
  let num_of_primes = 0
  let current_num = 2
  let prime_list = []

  while(true){
    let found_prime = true

    let current_num_root = Math.sqrt(current_num)

    for(var current_check of prime_list){
      if(current_check > current_num_root){
        break
      }

      if(current_num % current_check == 0){
        found_prime = false
        break
      }
    }

    if(found_prime){
      prime_list.push(current_num)
      num_of_primes++
      yield {prime: current_num, num: num_of_primes}
    }

    current_num++
  }
})()

let container = document.getElementById("prime-container")

let last_el

function create_el(container){
  let data = prime_generator.next().value
  last_el = <div>{data.num}: {data.prime}</div>
  container.appendChild(last_el)

  return last_el
}

let el1 = create_el(container), el2 = create_el(container)
let height = el2.getBoundingClientRect().top - el1.getBoundingClientRect().top

function calc_target_height(){
  return window.innerHeight * 5
}

let target_height = calc_target_height()

function test_el(){
  if(last_el.getBoundingClientRect().top < target_height){
    add_els(Math.ceil(target_height / height))
  }
}

function add_els(num){
  let fragment = document.createDocumentFragment()
  for(let i = 1; i <= num; i++){
    create_el(fragment)
  }
  container.appendChild(fragment)
}

test_el()

document.addEventListener("scroll", test_el)

window.addEventListener("resize", () => {
  target_height = calc_target_height()
  test_el()
})

/* exported goto */
function goto(num){
  window.scrollTo(0, num * height)
}
