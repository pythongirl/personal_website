prime_generator = (->
  num_of_primes = 0
  current_num = 2
  prime_list = []

  while true
    found_prime = true
    current_num_root = Math.sqrt current_num

    for current_check in prime_list
      break if current_check > current_num_root 
      
      if current_num % current_check == 0 
        found_prime = false
        break
    
    if found_prime
      prime_list.push current_num 
      num_of_primes++
      yield { prime: current_num, num: num_of_primes }

    current_num++
)()
  
container = document.getElementById "prime-container"

last_el = null

create_el = (container) ->
  data = prime_generator.next().value
  last_el = <div>{data.num}: {data.prime}</div>
  container.appendChild last_el


el1 = create_el container
el2 = create_el container

height = el2.getBoundingClientRect().top - el1.getBoundingClientRect().top

calc_target_height = -> window.innerHeight * 5

target_height = calc_target_height()

add_els = (num) ->
  fragment = document.createDocumentFragment()
  for i in [1..num]
    create_el fragment
  container.appendChild fragment

test_el = ->
  if last_el.getBoundingClientRect().top < target_height
    add_els Math.ceil target_height / height

test_el()

document.addEventListener "scroll", test_el

window.addEventListener "resize", ->
  target_height = calc_target_height()
  test_el()

