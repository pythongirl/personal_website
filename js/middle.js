var style_string = "display:flex;height:100%;"
var center_vert = "align-items:center;"
var center_horiz = "justify-content:space-around;"

class Middle extends HTMLElement {
  constructor() {
    super()
    this.container = document.createElement("div")
    this.container.setAttribute("style", style_string)
    this.container.appendChild(document.createElement("div")).appendChild(document.createElement("slot"))
    this.attachShadow({mode: "open"}).appendChild(this.container)
  }
}

customElements.define("middle-vh",
  class extends Middle {
    constructor(){
      super()
      this.container.setAttribute("style", this.container.getAttribute("style") + center_horiz + center_vert)
    }
  }
)

customElements.define("middle-v",
  class extends Middle {
    constructor(){
      super()
      this.container.setAttribute("style", this.container.getAttribute("style") + center_vert)
    }
  }
)

customElements.define("middle-h",
  class extends Middle {
    constructor(){
      super()
      this.container.setAttribute("style", this.container.getAttribute("style") + center_horiz)
    }
  }
)