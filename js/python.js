'use strict';

function load_script(url){
  return new Promise(function(resolve, reject){
    var script = document.createElement("script")
    script.type = "text/javascript"
    script.onload = resolve
    script.onerror = reject
    document.head.appendChild(script)
    script.src = url
  })
}

function show_python_error(err){
  var error_message = ""
  error_message += "Traceback (most recent call last):\n"
  error_message += err.traceback.map(function(c){
    return `  File "${c.filename}", line ${c.lineno}` // Column number is available is c.colno, but is inaccurate
  }).join("\n")
  error_message += `\n${err.tp$name}: ${Sk.ffi.remapToJs(err.args)}`
  console.error(error_message)
}

(function(){
	async function run(code, name){
    var lines = code.split(/\r?\n/)
    var begin_position = 0
    
    // Finds the position of the first non-whitespace character on the first non-whitespace line
    for(var i = 0; i < lines.length; i++){
      if(lines[i].match(/[^\s]/)){
        begin_position = lines[i].search(/\S/)
        break
      }
    }

    if(begin_position != -1){
      lines = lines.map(l => l.slice(begin_position))
      code = lines.join('\n')
    }

    return Sk.misceval.asyncToPromise(function(){
      return Sk.importMainWithBody(name, false, code, true)
    })
  }

	function init(){
    // Use Sk.externalLibraries to use other files
    // ex: Sk.externalLibraries = {show: {path: 'show.js'}}

    Sk.configure({
      output: console.log,
      read: (x) => {
        if(Sk.builtinFiles === undefined || Sk.builtinFiles["files"][x] === undefined){
          throw `File not found: '${x}'`
        }
        return Sk.builtinFiles["files"][x]
      }
    })

		var tags = Array.from(document.getElementsByTagName('script')).filter(e => e.type == "text/python")

		for(var t = 0; t < tags.length; t++){
			if(tags[t].src){
        var url = tags[t].src

        fetch(url).then(res => {
          return res.text()
        }).then(code => {
          var name
          try {
            name = url.match(/([^\/]+)(?=\.\w+$)/)[0]
          } catch(TypeError) {
            name = "<stdin>"
          }
          return run(code, name)
        }).then(res => {
          //console.log("Script success.")
        }).catch(show_python_error)
			} else {
				run(tags[t].text, "<stdin>").then(res => {
          //console.log("Script success.")
        }, show_python_error)
			}
		}
	}

	function main(){
    load_script("https://gisch.tk/plugin/skulpt/skulpt.min.js").then(res => {
      return load_script("https://gisch.tk/plugin/skulpt/skulpt-stdlib.js")
    }).then(res => {
      init()
    }).catch(err => {
      console.error("Could not load skulpt scripts.")
    })
    window.python = {run, init}
	}

	main()
})()