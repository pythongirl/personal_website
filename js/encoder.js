/* global _ */

let alphabet = _.range(32).toString(32).join("")

function generate_pair(offset){
  return String.fromCodePoint(0x300 + offset, 0x300 + offset + 1)
}

let n = _.range(32).map(n => generate_pair(n * 2))


function print_n(base, num, len){
  let b32enc = num.toString(32).padStart(len, "0").split("").reverse()  
  let nums = b32enc.map(b => n[alphabet.indexOf(b)])
  return base + nums.join("")
}

function random_block(len){
  let p = document.createElement("p")

  let metadata_unenc = new Uint8Array(len)
  crypto.getRandomValues(metadata_unenc)
  let metadata = Array.from(metadata_unenc).map(d => (d & 0b11111).toString(32).toUpperCase())
  console.log("Metadata length:",metadata.length)

  let data = new Uint32Array(len)
  crypto.getRandomValues(data)
  
  for(let i = 0; i < len; i++){
    p.innerText += print_n(metadata[i], data[i])
  }

  return p
}

_.range(16).forEach(() => { document.getElementById("output").appendChild(random_block(128))})
