/* exported perfect_center */
function perfect_center(contents, {vertical = true, horizontal = true}){
  let styleString = ""

  if(vertical) styleString += "align-items: center;"
  if(horizontal) styleString += "justify-content: space-around;"

  return <div style="display: flex; height: 100%;">
    <div style={styleString}>
      {contents}
    </div>
  </div>
}