let sections = {
  "Biology": [
    {q: "What is the only bird that can fly backwards?", a: "Hummingbird"},
    {q: "What is hyperlipidemia?", a: "High presence of fat in blood"},
    {q: "What meat on a roast turkey has more calories?", a: "Dark"}
  ],
  "Chemistry": [
    {q: "What is the melting point of Gallium?", a: "86 degrees Fahrenheit or 30 degrees Celsius"},
    {q: "How is bronze made?", a: "three parts copper and one part tin"},
    {q: "How is Invar made?", a: "36 percent nickle and 64 percent iron"},
    {q: "How is electrum made?", a: "one part gold one part silver"},
    {q: "What cobalt isotope is a byproduct of nuclear fission?", a: "Cobalt 60"},
    {q: "What form is the oxygen we breathe?", a: "Diatomic oxygen"},
    {q: "What is special about the allow Nitinol?", a: "It exhibits shape memory"},
    {q: "What isotopes are found in meteorites?", a: "Iron 60 and nickle 60"},
    {q: "What is steel made of?", a: "Iron and Carbon"},
    {q: "What is the alloy constantan made of?", a: "55 percent copper and 45 percent nickle"},
    {q: "What is the lightest metal?", a: "Lithium"},
    {q: "What color is copper oxide?", a: "Green"},
    {q: "What color are the vanadium oxidation states?", a: "Lilac, green, blue, yellow"}
  ],
  "Culture": [
    {q: "Which Star Trek character has appeared in the most episodes?", a: "Worf"},
    {q: "What was the last Super Bowl to be held in a college stadium?", a: "Super Bowl XXX in 1996 at the Sun Devil Stadium in Tempe, Arizona"}
  ],
  "Geography": [
    {q: "In what country is Dalmatia?", a: "Croatia"},
    {q: "Where is the geographical center of Croatia?", a: "Bosnia and Herzegovina"},
    {q: "What is the most American Mountain?", a: "Mount Evans because it is the highest mountain with a paved road all the way to the top."},
    {q: "What is the height of Matterhorn?", a: "14,692 feet"},
    {q: "What is the definition of the nautical mile?", a: "One sixtieth of one degree of lattitude (one arc-minute of latitude)"},
    {q: "Where is the land based pole of inaccesablility of North America", a: "Allen, South Dakota"},
		{q: "What town in the US has the highest zip code?", a: "Clarkson, WA"}
  ],
  "History": [
    {q: "Who was the first US-born president?", a: "Martin Van Buren"},
    {q: "Who are the 10 Greatest Brittons?", a: "Winston Churchill, Isambard Brunel, Princess Diana, Charles Darwin, William Shakespeare, Isaac Newton, Queen Elizabeth I, John Lenin, Horatio Nelson, and Oliver Cromwell"},
    {q: "Who were the first two countries to recognize Ukraine's independence from the Soviet Union?", a: "Canada and Poland"},
    {q: "What was the hottest event of 1666?", a: "The Great Fire of London"},
    {q: "When was the last invasion of mainland Britain?", a: "1797 by Napoleon"},
    {q: "In what year did the Cuban Missile Crisis happen?", a: "1962"},
    {q: "Who was the first catholic president?", a: "John F. Kennedy"},
    {q: "What animal was Jimmy Carter attacked by in 1979?", a: "A rabbit"},
    {q: "During what year was the Norweigan Butter Crisis?", a: "2011"},
    {q: "What is the fastest plane and how fast does it fly?", a: "X-15 at mach 6.72 (2,021 meters per second)"},
    {q: "What did Jack Churchill wield during world war two?", a: "Claymore, Bagpipes, and Longbow"},
    {q: "Who was the first navy veteran to become president?", a: "John F. Kennedy"},
	{q: "How did Isadora Duncan die?", a: "Her long scarf got entangled in the wheel of the car she was riding in and it broke her neck."},
	{q: "What were Thomas Midgley Jr.'s contributions to science?", a: "Leaded gasoline and CFC's"},
  ],
  "Mathematics": [
    {q: "Addition in a log scale results in what operation?", a: "Multiplication"}
  ],
  "Physics": [
    {q: "What is the speed of sound in meters per second?", a: "343 meters per second"},
    {q: "What is the TNT equivalent of energy released by the eruption of Mt. Krakatoa in 1883?", a: "200 megatons (largest nuclear bomb is 50 megatons)"}
  ],
  "Space": [
    {q: "What is the radius of the moon?", a: "1737 kilometers"},
    {q: "What is Mercury's orbital period?", a: "88 days"},
    {q: "What is the weight of the Curiosity mars rover?", a: "899 kilograms"},
    {q: "What spacecraft has the speed record as of 2018? How fast?", a: "Parker Solar Probe at 953,100 meters per second (213,200 miles per hour)"},
    {q: "Who was the first woman in space?", a: "Valentina Tereshkova"}
  ]
}

let tableOfContents = <ol></ol>
let questions = <dl></dl>

for(let section in sections){
  console.log(`Section: ${section}`)
  tableOfContents.appendChild(<li><a href={"#" + section}>{section}</a></li>)

  questions.appendChild(<h2 id={section}>{section}</h2>)

  for(let question of sections[section]){
    console.log(`Question: ${question.q}`)
    questions.appendChild(<>
      <dt>{question.q}</dt>
      <dd>{question.a}</dd>
    </>)
  }
}

document.getElementById("output").appendChild(<>
  <h2>Sections</h2>
  {tableOfContents}
  {questions}
</>)
