/* global $ */

/*function new_barcode() {
  $(".barcode").empty()
  $(".decoded").empty()
  var text = "*0" + (Math.floor(Math.random() * 90000000) + 10000000).toString() + "*"
  for (i = 0; i < 11; i++) {
    var myclass = "l" + i.toString()
    $(".barcode").append("<span class='code " + myclass + "'>" + text[i] + "</span>")
    $(".decoded").append("<span class='code " + myclass + "'>" + text[i] + "</span>");
    (function () {
      var myclass_t = myclass
      $("." + myclass_t).hover(function () {
        $(".code").not("." + myclass_t).removeClass("red")
        $("." + myclass_t).toggleClass("red")
      })
    })()
  }
}*/

let encoded_barcode = document.getElementById("barcode")
let decoded_barcode = document.getElementById("decoded")

function new_barcode(){
  encoded_barcode.innerHTML = ""
  decoded_barcode.innerHTML = ""

  let text = "*0" + (Math.floor(Math.random() * 90000000) + 10000000).toString().padStart(8, "0") + "*"
  for (let i = 0; i < 11; i++){
    encoded_barcode.appendChild(<span>{text[i]}</span>)
    decoded_barcode.appendChild(<span>{text[i]}</span>)
  }
}

$(document).ready(function () {
  $(".decoded").hide()
  new_barcode()
  $(".hide-button").hide()
  $(".toggle-button").click(function () {
    $(".toggle-button").toggle()
    $(".decoded").toggle()
  })
  $(".new-button").click(function () {
    new_barcode()
  })
})
