function speed(rpm){
  return rpm/(-13.5/100)+100;
}

function current(rpm){
  return rpm/(13.5/3.4)+0.2;
}

function power(x){
  speed_rads = speed(x)*2*Math.PI/60; // Convert RPM to rad/s
  torque_nm = x/8.8507;
  return speed_rads*torque_nm;
}

function efficiency(x){
	return power(x)/current(x);
}

var margin = {top: 50, right: 50, bottom: 70, left: 50};
var width = 800 - margin.left - margin.right;
var height = 550 - margin.top - margin.bottom;


var xScale = d3.scaleLinear().domain([0, 13.5]).range([0, width]);
var yScale = d3.scaleLinear().domain([0, 4.3]).range([height, 0]);
var speedScale = d3.scaleLinear().domain([0, 120]).range([height, 0]);

function my_line(x_scale, y_scale){
	return d3.line()
		.x(function(d){ return x_scale(d.x); })
		.y(function(d){ return y_scale(d.y); })
		.curve(d3.curveMonotoneX);
}

// 8. An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
var datasetPower = d3.range(28).map(function(d){ return {"y": power(d/2) , "x": d/2}; });
var datasetCurrent = d3.range(28).map(function(d){ return {"y": current(d/2), "x": d/2}; });
var datasetSpeed = d3.range(28).map(function(d){ return {"y": speed(d/2), "x": d/2}; });
var datasetEfficiency = d3.range(28).map(function(d){ return {"y": efficiency(d/2)/1.8, "x": d/2}; });

// 1. Add the SVG to the page and employ #2
var svg = d3.select("#image-insert")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g") // Window
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Create x axis
svg.append("g").attr("transform", "translate(0," + height + ")").call(d3.axisBottom(xScale));
// Label
svg.append("text")
	.style("font-weight","700")
	.attr("transform", "translate(" + (width/2) + " ," + (height + margin.top) + ")")
	.style("text-anchor", "middle")
	.text("Torque (in-lbs)");

// Create speed y axis
svg.append("g").call(d3.axisLeft(speedScale));
// Label
svg.append("text")
	.style("font-weight","700")
	.attr("transform", "rotate(-90)")
	.attr("y", 0 - margin.left)
	.attr("x",0 - (height / 2))
	.attr("dy", "1em")
	.style("text-anchor", "middle")
	.text("Speed (rpm)");  
 
// Create current / power y axis
svg.append("g").attr("transform", "translate( " + width + ", 0 )").call(d3.axisRight(yScale));
// Label
svg.append("text")
	.style("font-weight","700")
	.attr("transform", "rotate(90)")
	.attr("y", 0-(width+margin.left))
	.attr("x", (height / 2)).attr("dy", "1em")
	.style("text-anchor", "middle")
	.text("Power & Current");

// Title
svg.append("text")
	.attr("x", width / 2).attr("y", 0-margin.top)
	.attr("dy", "1em")
	.style("font-weight", "700")
	.style("text-anchor", "middle")
	.text("Torque-speed curve - 2 wire 393");
// Source
svg.append("text").attr("x", (width)).attr("y", (height + margin.top))
	.style("text-anchor", "end")
	.style("font-size", "9pt")
	.text("Graph based on ")
	.append("a")
		.attr("href", "https://www.vexforum.com/index.php/7868-motor-torque-speed-curves/0")
		.attr("target", "_blank")
		.text("James Pearman's graph.")
		.style("fill", "blue")
		.style("text-decoration", "underline");

// Add data
svg.append("path").datum(datasetPower).attr("class", "line")
	.attr("d", my_line(xScale, yScale))
	.style("stroke", "firebrick").style("stroke-dasharray", "10px");
svg.append("path").datum(datasetSpeed).attr("class", "line")
	.attr("d", my_line(xScale, speedScale))
	.style("stroke", "hsl(330deg, 100%, 45%)");
svg.append("path").datum(datasetEfficiency).attr("class", "line")
	.attr("d", my_line(xScale, yScale))
	.style("stroke", "maroon");
svg.append("path").datum(datasetCurrent).attr("class", "line")
	.attr("d", my_line(xScale, yScale))
	.style("stroke", "blue");