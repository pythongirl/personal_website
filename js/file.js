CodeMirror.modeURL = "/plugin/codemirror/mode/%N/%N.js";

$(() => {
	editor = CodeMirror.fromTextArea($('.main')[0], {
		lineNumbers: true
	});

	$('#save-btn').click(() => {
		console.log(editor.getValue());
	});

	if(window.location.hash && /[A-Za-z\-_]+/.test(window.location.hash)){
		$.get('/edit/raw?p=' + window.location.hash.slice(1)).done((data) => {
			editor.setValue(data);
		});

		var decoded_path = Base64.decode(window.location.hash.slice(1));
		var file_extension = /(?:\.([^.]+))?$/.exec(decoded_path)[1];
		var mode = CodeMirror.findModeByExtension(file_extension);
		editor.setOption('mode', mode.mime);
		CodeMirror.autoLoadMode(editor, mode.mode);

	}
});

