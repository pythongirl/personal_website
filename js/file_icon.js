var extensions = {
    "7z": "compressed",
    "bin": "binary",
    "c": "c",
    "css": "a",
    "csv": "odf6ods",
    "dmg": "diskimg",
    "gif": "image2",
    "gz": "compressed",
    "html": "layout",
    "ico": "image2",
    "iso": "diskimg",
    "jpeg": "image2",
    "jpg": "image2",
    "js": "script",
    "mp4": "movie",
    "patch": "patch",
    "pdf": "pdf",
    "pl": "p",
    "png": "image2",
    "ps": "ps",
    "py": "p",
    "rar": "compressed",
    "svg": "svg",
    "tar": "tar",
    "tif": "image2",
    "tiff": "image2",
    "ttf": "a",
    "txt": "text",
    "wav": "sound1",
    "xml": "xml",
    "z": "compressed",
    "zip": "compressed"
};

function get_icon(filename){
    // console.log("Checking '" + filename + "'");
    var extension = '';
    var m;
    if (m = filename.match(/[^\\]*\.(\w+)$/)){
        extension = m[1];
    }
    // console.log("Extension: '" + extension + "'");
    if(extension in extensions){
        return extensions[extension];
    } else {
        return 'generic';
    }
}

if(typeof exports !== 'undefined'){
    exports.extensions = extensions;
    exports.get_icon = get_icon;
}
