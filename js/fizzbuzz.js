window.appvars = {}

var container = document.getElementById("container")

appvars.counter = 1


function create_el(){
  var el = document.createElement("p")

  var number = appvars.counter++

  var message = ""

  if(number % 3 == 0) message += "Fizz"
  if(number % 5 == 0) message += "Buzz"
  if(!message) message = number.toString()

  el.innerHTML = message
  container.appendChild(el)

  return el
}

var last_el = create_el()

function test_el(){
  if(last_el.getBoundingClientRect().top < (window.innerHeight * 1.1)){
    last_el = create_el()
    test_el()
  }
}

test_el()

document.onscroll = function(e){
  test_el()
}