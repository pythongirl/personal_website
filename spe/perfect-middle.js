export default {
  name: 'perfect-middle',
  template: `<div style="display:flex;align-items:center;justify-content:space-around;height:100%;">
    <div>
      <slot/>
    </div>
  </div>
  `
}