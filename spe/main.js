import PerfectMiddle from "./perfect-middle.js"


function escapeHtml(unsafe) {
  return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
}

let app = new Vue({
  "el": "#app",
  data: () => ({
    plans: [],
    active_tab: 0
  }),
  components: {
    PerfectMiddle
  }
})

window.app = app

class PlanDrawer {
  constructor(plan, draw){
    this.plan = plan
    this.draw = draw
    this.max_y = 0
  }

  draw_node(node, x, y, bottom){
    let node_id = node.getAttribute("NodeId")
    let physical_op = node.getAttribute("PhysicalOp")
    let logical_op = node.getAttribute("LogicalOp")

    console.log("drawing node " + node_id)

    let operation = Array.from(node.children).filter(e => !(["OutputList", "MemoryFractions", "RunTimeInformation"].includes(e.tagName)))[0]
    let branches = Array.from(operation.children).filter(e => e.tagName == "RelOp")

    if(bottom){
      y = this.max_y + 50
      this.max_y = y
    }

    let node_repr = this.draw.rect(45, 45).attr({fill: '#f22', 'x': x, 'y': y})
    let node_label = this.draw.text(`${physical_op}\n(${logical_op})`).attr({'x': x, 'y': y, 'font-size': 12})

    for(var i = 0; i < branches.length; i++){
      this.draw_node(branches[i], x + 100, y, i != 0)
    }
  }
}

class Plan {
  constructor(name, text){
    this.name = name
    this.text = text

    this.doc = new DOMParser().parseFromString(this.text, "text/xml")
  }

  statement_strings(){
    return Array.from(this.doc.getElementsByTagName('StmtSimple')).map(
      e => escapeHtml(e.getAttribute("StatementText")).trim()
    )
  }

  render_query_plan(){
    let query_plan = this.doc.getElementsByTagName('QueryPlan')[0]
    let first_op = Array.from(query_plan.children).filter(e => e.tagName == "RelOp")[0]

    var draw = SVG('drawing-0').size(window.innerWidth,300)
    
    let plan_drawer = new PlanDrawer(this, draw)

    plan_drawer.draw_node(first_op, 0, 0, false)


  }
}

function handle_file(file){
  const reader = new FileReader()
  reader.onload = function(){
    app.plans.push(new Plan(file.name, reader.result))
  }
  reader.readAsText(file)
}

function handle_files(files){
  Array.from(files).forEach(v => {
    handle_file(v)
  })
}

let file_input = document.querySelector('#file-input')

file_input.addEventListener('change', function(e){
  handle_files(file_input.files)
}, false)

document.addEventListener('dragover', function(e){
  e.preventDefault()
  e.stopPropagation()
}, false)

document.addEventListener('drop', function(e){
  e.preventDefault()
  e.stopPropagation()
  handle_files(e.dataTransfer.files)
}, false)